<?php
/**
 *
 * @package   Outdated_Browser_Plugin
 * @author    Yan Knudtskov Nielsen <yan@vires-artes.dk>
 * @license   GPL-2.0+
 * @link      http://vires-artes.dk
 * @copyright 2014 Vires Artes
 *
 * @wordpress-plugin
 * Plugin Name:       Outdated Browser Plugin
 * Plugin URI:        http://vires-artes.dk
 * Description:       This is the Outdated Browser Plugin. It is based on work by http://outdatedbrowser.com/ and https://github.com/tareq1988
 * Version:           1.0.0
 * Author:            Yan Knudtskov Nielsem, Original idea by Tareq Hasan
 * Author URI:        http://vires-artes.dk
 * Text Domain:       va-outdated-browser-plugin
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * GitHub Plugin URI: https://github.com/yanknudtskov/outdated-browser-plugin
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-outdated-browser-plugin.php' );
register_activation_hook( __FILE__, array( 'Outdated_Browser_Plugin', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Outdated_Browser_Plugin', 'deactivate' ) );
add_action( 'plugins_loaded', array( 'Outdated_Browser_Plugin', 'get_instance' ) );
