<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   Outdated_Browser_Plugin
 * @author    Yan Knudtskov Nielsen <yan@vires-artes.dk>
 * @license   GPL-2.0+
 * @link      http://vires-artes.dk
 * @copyright 2014 Vires Artes
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// @TODO: Define uninstall functionality here